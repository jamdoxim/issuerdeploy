# core360demo linked cem microservice configuration
# This los integration test system is running the CEM communication microservice
# linked to the dxmcem test issuer on core360demo

set channel_id "JamTest_UAT-DoximComm_TEST_CAEast"

::md::optiondb add "ocmd.channel.${channel_id}.type"    "ecEvent"
::md::optiondb add  "ocmd.channel.${channel_id}.name"   "JamTest_UAT-DoximComm_TEST_CAEast ecEvent"

# DOC: We can lock this channel to a particular issuer.  We have multiple CEM issuers on the demo
# that can share this channel right now so we don't set the issuer id though the channel is named
# for the main development CEM issuer.
::md::optiondb add  ocmd.channel.${channel_id}.issuer_id     "jamtest"

::md::optiondb add  ocmd.channel.${channel_id}.webhook_url   "https://los-staging2.doxim.com/DoximComm_MSV_TEST_CAEast/api/communication/auto-assign"
::md::optiondb add  ocmd.channel.${channel_id}.auth.mode     "cem"
::md::optiondb add  ocmd.channel.${channel_id}.auth.user     "tamtest_cem_uat"
::md::optiondb add  ocmd.channel.${channel_id}.auth.password "H8s7&STtftfs555S7ys8u8sjiah"

::md::optiondb add  ocmd.channel.${channel_id}.auth.cem.token_api_url "https://los-staging2.doxim.com/DoximComm_MSV_TEST_CAEast/token"
::md::optiondb add  ocmd.channel.${channel_id}.auth.cem.allowed_clock_skew 30

::md::optiondb add  ocmd.channel.${channel_id}.auth.cem.rsa_public_key "PFJTQUtleVZhbHVlPjxNb2R1bHVzPnhWNzcwdHpETkxpS0h5QXcxdlZTc00rRVJEZDhWWW1PamZseGg0MnkxSTVnNXJYSDhJa3VlY3M0NDZjSzVMNU1qWHUrMmROZm1KOEtvRndiQ2hEV3Nkd0xnYVphRVNjOHl3N2hoTmgrdk5vdnhFQythWjZMZmJFT1pvcDg5c0JzQ3hvRGxrVUNKSktGUkJpeEtjZ0ZGTWxjQ2o5UWtOeXRmVUFLWCtDdkt0RFFLQmRoWEpEVzR3amcrd0xoTmMzamErWmwrWHFQVUl4cXFVY1hoaUxoR05oSzltZ09uUHd5aGJKWjBkWFNDTzFic3dCaXBwdlBvLzA5SnhFcDQ2T21NOFROay91azI2YlVWdTl4SDkxdzRhZm90QnJOUHR5UE92eDJZNUs2Z1lDZXpsY29sZFl2Rk8zTm05RGhBdE1mUmIyYkdlMDNrQzJQYk0yUHdtSFd3dz09PC9Nb2R1bHVzPjxFeHBvbmVudD5BUUFCPC9FeHBvbmVudD48L1JTQUtleVZhbHVlPg=="
