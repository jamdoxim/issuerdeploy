#!/bin/bash

TIMESTAMP=$(date)

SAAS_CONF="/home/ecourier/config/jamtest1_saas_issuer.conf.tcl"

if [ -f /home/ecourier/config/fusion_saas_issuer.conf.tcl ]; then
        error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Config file [/home/ecourier/config/jamtest1_saas_issuer.conf.tcl] already exists!"
        echo $error
        echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
        echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
        exit 1
fi

cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then 
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error running [cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl /home/ecourier/config/jamtest1_saas_issuer.conf.tcl]"
	echo $error
       	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
       	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
       	exit 1
fi

sed -i '9s/.*/set issuer_id "jamtest1"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then 
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 9]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '10s/.*/set mail_domain "jamtest1.doximapps.com"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then 
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 10]"
	echo $error
       	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
       	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
       	exit 1
fi

sed -i '13s/.*/md::optiondb add issuer.entity.fullname     "Jam"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 13]"
	echo $error
	echo $error >>  "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '14s/.*/md::optiondb add issuer.entity.description  "Jam"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 14]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '15s/.*/md::optiondb add issuer.entity.country_code CA/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then 
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 15]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '17s/.*/md::optiondb add issuer.administrator.principal  "jam.legrand"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 17]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '18s/.*/md::optiondb add issuer.administrator.first_name "Jam"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then 
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 18]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '19s/.*/md::optiondb add issuer.administrator.last_name  "Legrand"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 19]"
	echo $error
.	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '22s/.*/md::optiondb add issuer.administrator.password      "r0BF92j7Tn6.T9JY7G!4"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 22]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '23s/.*/md::optiondb add issuer.database.user.rw.password   "HusT6dUjs9Ijsgfd5SJEI8"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 23]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '24s/.*/md::optiondb add issuer.database.user.ro.password   "Yu8iKhy6FDs9Jd6shHFP"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 24]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

sed -i '28s/.*/md::optiondb add issuer.ports.start_port  "next_available_port"/' /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error modifying [line 28]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

chown ecourier: /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error running [chown ecourier: /home/ecourier/config/jamtest1_saas_issuer.conf.tcl]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

chmod 644 /home/ecourier/config/jamtest1_saas_issuer.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error running [chmod 644 /home/ecourier/config/jamtest1_saas_issuer.conf.tcl]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

chown ecourier: /home/jaml@cac1prod.doxim.net/jamtest1_definition.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error running [chown ecourier: /home/jaml@cac1prod.doxim.net/jamtest1_definition.conf.tcl]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log; exit 1
fi

chmod 644 /home/jaml@cac1prod.doxim.net/jamtest1_definition.conf.tcl
if [ ! $? == 0 ]; then
	error="$TIMESTAMP - [ERROR] - Failed to create issuer jamtest1 - Error running [chmod 644 /home/jaml@cac1prod.doxim.net/jamtest1_definition.conf.tcl]"
	echo $error
	echo $error >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	echo "1" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
	exit 1
fi

echo "$TIMESTAMP - [OK] - Successfully created issuer: [jamtest1]";
echo "0" >> /home/jaml@cac1prod.doxim.net/deploy-jamtest1.log
exit 0
