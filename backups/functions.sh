init ()
{
        yaml=$1

        # Validate command line arguments
        log_message "Validating provided arguments." "OK" "FILE" "" 1
        if [ ! -z $error ]; then
                log_message "Please only provde one argument. [YAML file path]" "ERROR" "FILE" "" 1
                return 1
        fi

        # Check for Python
        pythonVersion=$(python --version)
        if [[ ! $pythonVersion == *"Python 3"* ]]; then
                log_message "Script requires Python 3 or higher." "ERROR" "FILE" "" 1
                return 1
        fi

        # Verify YAML file
        if [ -z $yaml ]; then
                log_message "Specified file [$yaml] does not exist or is not readable." "ERROR" "FILE" "" 1
                return 1
        fi

	if ! validate_data $yaml; then
		return 1
	fi

        if ! process_yaml $yaml; then
		return 1
	fi

	if ! yaml_data; then
		return 1
	fi
	
	return 0
}

process_yaml ()
{
        yaml=$1

	log_message "Processing YAML file: [$yaml]" "INFO" "FILE" "" 1

	if [ ! -e $yaml ]; then
		log_message "Provided YAML file does not exist: [$yaml]" "ERROR" "FILE" "" 1
		return 1
	fi
	
	log_message "Processing YAML file: [$yaml]" "OK" "FILE" "" 1
	process=$(python -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < $yaml 2>&1)
        
	if [[ $process == *"Traceback"* ]] || [[ $process == *"No such file"* ]]; then
                log_message "Failed to process [$yaml]." "ERROR" "FILE" "" 1
                return 1
        else
		log_message "Processed YAML file: [$yaml]" "OK" "FILE" "" 1
		export yamlData=$process
	fi
	
	return 0
}

validate_data ()
{
        # YAML value sanitation and verification
        yaml=$1

	log_message "Validating data in provided YAML file: [$yaml]" "INFO" "FILE" "" 1

	testYaml=$(python -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < $yaml 2>&1)

        if [[ $testYaml == *"Traceback"* ]]; then
                log_message "Specified file [$yaml] contains errors." "ERROR" "FILE" "" 1
                return 1
	fi

	log_message "Provided YAML file validated: [$yaml]" "OK" "FILE" "" 1

	return 0
}

yaml_data ()
{
	log_message "Attempting to process YAML Data" "INFO" "FILE" "" 1

	python='exec("""\nimport json,sys\ndef A():\n    C=str(sys.argv[1]);A=json.loads(C)\n    for B in A:D=A[B];print("{}={}".format(B,D)\n)\nif __name__=="__main__":A()\n""")'

	echo $python > $BASE_DIRECTORY/temp.py

	json=$(echo $yamlData | sed 's/'\''/"/g')

	data=$(python $BASE_DIRECTORY/temp.py "$json" 2>&1)

	if [[ $data == *"Traceback"* ]]; then
		log_message "Unable to process JSON data: $data" "ERROR" "FILE" "" 1
		return 1
	fi

	declare -A yamlArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		yamlArray[$key]=$value
	done

	export envPath=${yamlArray[envPath]}
	log_message "Enviroment Path: $envPath" "INFO" "FILE" "" 1
	
	export issuerId=${yamlArray[issuerId]}
	log_message "Issuer ID: $issuerId" "INFO" "FILE" "" 1

	export dbIp=${yamlArray[dbIp]}
	log_message "Database IP: $dbIp" "INFO" "FILE" "" 1

	export mtaIp=${yamlArray[mtaIp]}
	log_message "Mta IP: $mtaIp" "INFO" "FILE" "" 1
		
	export webIp=${yamlArray[webIp]}
	log_message "Web IP: $webIp" "INFO" "FILE" "" 1
		
	export appIp=${yamlArray[appIp]}
	log_message "App IP: $appIp" "INFO" "FILE" "" 1
		
	export docgenIp=${yamlArray[docgenIp]}
	log_message "Docgen IP: $docgenIp" "INFO" "FILE" "" 1

	export vpnUser=${yamlArray[vpnUser]}
	log_message "VPN Username: $vpnUser" "INFO" "FILE" "" 1

	export site=${yamlArray[site]}
	log_message "Deployment site: $site" "INFO" "FILE" "" 1

	export inboundRequestUrl=${yamlArray[inboundRequestUrl]}
	log_message "Inbound Request URL: $inboundRequestUrl" "INFO" "FILE" "" 1

	export twillioProductionSID=${yamlArray[twillioProductionSID]}
	log_message "Twillio account SID: $twillioProductionSID" "INFO" "FILE" "" 1
	
	export areaCode=${yamlArray[areaCode]}
	log_message "Area Code: $areaCode" "INFO" "FILE" "" 1

	export country=${yamlArray[country]}
	log_message "Country: $country" "INFO" "FILE" "" 1

        if [ ${yamlArray[mobetize]} == 'True' ]; then
		log_message "Mobetize Issuer = True - Note: manual steps required after completion." "INFO" "FILE" "" 1
		result=${yamlArray[mobetize]}
		export mobetize=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	if [ ${yamlArray[enableCem]} == 'True' ]; then
		log_message "CEM Enabled" "INFO" "FILE" "" 1
		result=${yamlArray[enableCem]}
		export enableCem=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	if [ ${yamlArray[enableEcm]} == 'True' ]; then
		log_message "ECM Enabled" "INFO" "FILE" "" 1
		result=${yamlArray[enableEcm]}
		export enableEcm=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	export siteLevelUser=${yamlArray[siteLevelUser]}
	export siteLevelPass=${yamlArray[siteLevelPass]}

	export vpnPass=${yamlArray[vpnPass]}
	export db2Pass=${yamlArray[db2Pass]}
	export issuerRWPass=${yamlArray[issuerRWPass]}
	export issuerROPass=${yamlArray[issuerROPass]}
	export twillioProductionToken=${yamlArray[twillioProductionToken]}
	export doximDomain=${yamlArray[doximDomain]}

	export issuerFullName=${yamlArray[issuerFullName]}
	export issuerDescription=${yamlArray[issuerDescription]}
	export adminPrincipal=${yamlArray[adminPrincipal]}
	export adminFirstName=${yamlArray[adminFirstName]}
	export adminLastName=${yamlArray[adminLastName]}
	export adminPass=${yamlArray[adminPass]}

	export deployStages=${yamlArray[deployStages]}

	export forceDelete=${yamlArray[forceDelete]}
	if [ $forceDelete == 'True' ]; then
		if ! force_delete_docgen; then
			log_message "Failed to force delete issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			exit 1
		fi
		if ! force_delete_db; then
			log_message "Failed to force delete db2 data for issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			exit 1
		fi
		exit 0
	fi

	issuerPort=next_available_port
	if [ $issuerPort == 1 ]; then
		log_message "Failed to gather next port for issuer: [${issuerId}].." "ERROR" "FILE" "" 1
		return 1
	fi

	export port=$issuerPort

	log_message "Successfully processed YAML data" "OK" "FILE" "" 1

	return 0
}

main ()
{
        yaml=$1

        if [ ! $yaml ]; then
		log_message "Empty string provided for YAML file." "ERROR" "FILE" "" 1
		return 1
	fi

	if [ $yaml == "help" ]; then
		help_menu
		exit 0
	fi

        if ! init $yaml; then
		return 1
	fi
	
	if ! expect_check; then
		return 1
	fi

	if ! rsa_verify; then
		return 1
	fi

	STAGE=0

	if [ $forceDelete == 'True' ]; then
		STAGE="FORCE_DELETE"
		if ! force_delete_docgen; then
			log_message "Failed to force delete issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			return 1
		fi
		if ! force_delete_db; then
			log_message "Failed to force delete db2 data for issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			return 1
		fi
		if [ $force_deploy_wars ]; then
			if ! redeploy_wars; then
				log_message "Failed to redploy wars: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
				return 1
			fi
		fi
		return 0
	fi

	STAGE=1

	if [[ $deployStages == *"1"* ]]; then 
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! database_users; then
			rollback $STAGE
			return 1
		fi
	fi

	# Below here are untested

	STAGE=2
	if [[ $deployStages == *"2"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! docgen_setup; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=3

	if [[ $deployStages == *"3"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! enable_cem; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=4

	if [[ $deployStages == *"4"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! enable_ecm; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=5

	if [[ $deployStages == *"5"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		# Will need to test next time a mobetize issuer is required.
		if ! mobetize; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=6

	if [[ $deployStages == *"6"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! twillio_subaccount; then
			rollback $STAGE
			return 1
		fi
	fi
	
	STAGE=7

	if [[ $deployStages == *"7"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! twillio_conf; then
			rollback $STAGE
			return 1
		fi
	fi
	STAGE=8

	if [[ $deployStages == *"8"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! redeploy_wars; then
			rollback $STAGE
			return 1
		fi
	fi

	return 0
}

rollback ()
{
	rollbackStage=$1

	if [ $rollbackStage == 1 ]; then
		if ! rollback_database_users; then
			log_message "Error attempting to roll back Stage $rollbackStage [Rollback Database Users]" "ERROR" "FILE" "" 1		
			return 1
		fi
	fi

        if [ $rollbackStage == 2 ]; then
                if ! rollback_docgen_setup; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Docgen Setup]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 3 ]; then
                if ! rollback_enable_cem; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Enable CEM]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 4 ]; then
                if ! rollback_enable_ecm; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Enable ECM]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 5 ]; then
                if ! rollback_mobetize; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Mobetize Setup]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 6 ]; then
                if ! rollback_twillio_subaccount; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Twillio Subaccount Creation]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 7 ]; then
                if ! rollback_twillio_conf; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback ]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 8 ]; then
                if ! rollback_redeploy_wars; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Database Users]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi
}

help_menu ()
{
	printf "Issuer Deployment Tool\n\n"
	printf "usage: ./awsdeploy [YAML Config File]   	launch awsdeploy\n\n"

	printf "Arguments:\n"
	printf "  help                        			Access help menu and yaml configuration example.\n"

	printf "Directory Structure:\n"
	printf "  _lib                                          Holds all functions and scripts\n"
	printf "  _tempaltes                                    Holds all YAML template files\n"

	printf "Issuer Deployment Tool from anywhere:\n"
	printf "  Ensure permissions are correct      		[chmod u+x awsdeploy]\n"
	printf "  Add the following command to your .bashrc     [alias awsdeploy='<AWSDEPLOY ROOT>/awsdeploy']\n\n"

	# Add configuration example
}

rsa_verify ()
{
	user=$(whoami)

	log_message "Attempting to verify RSA access to required servers" "INFO" "FILE" "" 1
	log_message "Current detected user: $user" "INFO" "FILE" "" 1

	export privateKey="/home/$user/.ssh/id_issuerDeploy_rsa"

	# Add password pipe for id copy
	if [ ! -e $privateKey ]; then
		log_message "Private Key [$privateKey] does not exist. generating RSA" "INFO" "FILE" "" 1
		result=$(ssh-keygen -t rsa -f $privateKey -P '' 2>&1)
		if [[ ! $result == *"randomart"* ]]; then
			log_message "OpenSSL failed to create RSA key: $result" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "OpenSSL key created: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$dbIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			echo "$result"
			log_message "Failed to copy SSH key [$privateKey] to remote host: $dbIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $dbIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$docgenIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $docgenIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $docgenIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$webIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $webIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $webIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$appIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $appIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $appIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$mtaIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $mtaIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $mtaIp: $result" "OK" "FILE" "" 1
		fi
	fi

	log_message "RSA file verification complete" "OK" "FILE" "" 1	
	
	return 0
}

expect_check ()
{
	log_message "Verifying required packages" "INFO" "FILE" "" 1
	
	unameOut="$(uname -s)"
	case "${unameOut}" in
    		Linux*)     machine=Linux;;
    		Darwin*)    machine=Mac;;
    		*)          machine="UNKNOWN:${unameOut}"
	esac

	if [[ $machine == "Linux" ]]; then
		if [ -e /etc/os-release ]; then
			if [ ! -e /usr/bin/expect ]; then
				log_message "Unable to find package 'expect', please install using: [apt-get install -y expect]" "ERROR" "FILE" "" 1
				return 1
			fi
		elif [ -e /etc/redhat-release ]; then
			if [ ! -e /usr/bin/expect ]; then
				log_message "Unable to find package 'expect', please install using: [yum install expect -y]" "ERROR" "FILE" "" 1
				return 1
			fi
		else
			log_message "Unable to determine Linux OS: expecting RedHat, or Ubuntu" "ERROR" "FILE" "" 1
			return 1
		fi
	elif [[ $machine == "Mac" ]]; then
		if [ ! -e /usr/bin/expect ]; then
			log_message "Unable to find package 'expect'" "ERROR" "FILE" "" 1
			return 1
		fi
	fi

	log_message "Required packages verified successfully" "OK" "FILE" "" 1

	return 0
}

database_users ()
{
	log_message "Attempting to create DB2 database users for DB Server: $dbIp using the username: $vpnUser" "INFO" "OK" "" 1
	#result=$(timeout 10 | ssh -i ${privateKey} ${vpnUser}@${dbIp} "su $issuerId" 2>&1)
	#if [[ ! "$result" == *"does not exist"* ]];then
	#	log_message "Failed to create DB2 database users for $issuerId - user already exists" "ERROR" "FILE" "" 1
	#	return 1
	#fi

	issuerRo=$( printf $issuerId;printf "ro" )

	commands=(
		". /home/db2inst1/sqllib/db2profile;"
		"cd /opt/md/custom/core360saas/bin/;"
		"./ecdb2_create_issuer_users ${envPath}/ecdb2_environment.sh ${issuerId} ${db2Pass};"
		"passwd ${issuerId}"
		"passwd ${issuerId}ro"
	)

	env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} source="${commands[0]}" changeDir="${commands[1]}" create="${commands[2]}" passOne="${commands[3]}" passTwo="${commands[4]}" issuerRw="${issuerRWPass}" issuerRo="${issuerROPass}" expect << "EOS" 
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout 4
	  expect "*s password:*"
	  send -- "$env(pass)\r"

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(create)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(passOne)\r"

          set timeout 4
          expect "*New password*"
          send -- "$env(issuerRw)\r"

          set timeout 4
          expect "*Retype new password*"
          send -- "$env(issuerRw)\r"

	  set timeout -1
          expect -re $rootPrompt
          send -- "$env(passTwo)\r"

          set timeout 4
          expect "*New password*"
          send -- "$env(issuerRo)\r"

          set timeout 4
          expect "*Retype new password*"
          send -- "$env(issuerRo)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
	if [ ! $? == 0 ]; then
		log_message "Failed to create DB2 database users for $issuerId" "OK" "FILE" "" 1
		return 1
	fi

	log_message "Successfully created DB2 database users for $issuerId" "OK" "FILE" "" 1
	return 0
}

docgen_setup ()
{
	log_message "Configuring DocGen server - Issuer Creation" "INFO" "OK" "" 1

	# Build locally, scp over, run create - pipe all script commands to log, not main command

	saasConf="/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
	#export cemConf="/home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"
	export cemConf="${issuerId}_definition.conf.tcl"

	result=$(scp -i ${privateKey} _cem/${cemConf} ${vpnUser}@${docgenIp}:~/)

	#logFile="/home/${vpnUser}@${doximDomain}/deploy-${issuerId}.log"

	script_data="#!/bin/bash

TIMESTAMP=\$(date)

SAAS_CONF=\"/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl\"

if [ -f /home/ecourier/config/fusion_saas_issuer.conf.tcl ]; then
        error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Config file [/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl] already exists!\"
        echo \$error
        echo \$error >> $logFile
        echo \"1\" >> $logFile
        exit 1
fi

cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl ${saasConf}]\"
	echo \$error
       	echo \$error >> $logFile
       	echo \"1\" >> $logFile
       	exit 1
fi

sed -i '9s/.*/set issuer_id \"${issuerId}\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 9]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '10s/.*/set mail_domain \"${issuerId}.doximapps.com\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 10]\"
	echo \$error
       	echo \$error >> $logFile
       	echo \"1\" >> $logFile
       	exit 1
fi

sed -i '13s/.*/md::optiondb add issuer.entity.fullname     \"${issuerFullName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 13]\"
	echo \$error
	echo \$error >> $logFileecho \"1\" >> $logFile
	exit 1
fi

sed -i '14s/.*/md::optiondb add issuer.entity.description  \"${issuerDescription}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 14]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '15s/.*/md::optiondb add issuer.entity.country_code ${country}/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 15]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '17s/.*/md::optiondb add issuer.administrator.principal  \"${adminPrincipal}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 17]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '18s/.*/md::optiondb add issuer.administrator.first_name \"${adminFirstName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 18]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '19s/.*/md::optiondb add issuer.administrator.last_name  \"${adminLastName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 19]\"
	echo \$error
.	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '22s/.*/md::optiondb add issuer.administrator.password      \"${adminPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 22]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '23s/.*/md::optiondb add issuer.database.user.rw.password   \"${issuerRWPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 23]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '24s/.*/md::optiondb add issuer.database.user.ro.password   \"${issuerROPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 24]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '28s/.*/md::optiondb add issuer.ports.start_port  \"${port}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 28]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chown ecourier: ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chown ecourier: ${saasConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chmod 644 ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chmod 644 ${saasConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chown ecourier: /home/${vpnUser}@${doximDomain}/${cemConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chown ecourier: /home/${vpnUser}@${doximDomain}/${cemConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile; exit 1
fi

chmod 644 /home/${vpnUser}@${doximDomain}/${cemConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chmod 644 /home/${vpnUser}@${doximDomain}/${cemConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

echo \"\$TIMESTAMP - [OK] - Successfully created issuer: [${issuerId}]\";
echo \"0\" >> $logFile
exit 0"

        file="${PWD}/script_tmp-docgen.sh"
	remoteFile="/home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"

	echo -e "$script_data" > $file

        if [ ! $? == 0 ];then
                log_message "Failed to create issuer: $issuerId - Unable to write to $file" "ERROR" "FILE" "" 1
                return 1
        fi

        result=$(scp -i ${privateKey} ${file} ${vpnUser}@${docgenIp}:~/)

        if [ ! $? == 0 ];then 
		log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	commands=(
		"chmod +x $remoteFile;" 
		"/usr/bin/bash /home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh >> /home/${vpnUser}@${doximDomain}/deploy-${issuerId}.log;" 
		"su - ecourier"
		". /etc/md/${site}/ecourier_shellrc"
		"cd /opt/md/custom/core360saas/bin/"
		"./ec_create_issuer ${saasConf}"
	)

	env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} chmod="${commands[0]}" script="${commands[1]}" su="${commands[2]}" source="${commands[3]}" dir="${commands[4]}" create="${commands[5]}" siteUser="${siteLevelUser}" sitePass="${siteLevelPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout 4
          expect "*s password:*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(chmod)\r"

          set timeout -1
	  expect -re $rootPrompt          
	  send -- "$env(script)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(dir)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(create)\r"

          set timeout -1
          expect "*Enter your site level CORE360 username*"
          send -- "$env(siteUser)\r"

          set timeout -1
          expect "*Password: *"
          send -- "$env(sitePass)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"
 
          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	result=$(ssh -o LogLevel=QUIET -t -i ${privateKey} ${vpnUser}@${docgenIp} "cat ${logFile} | tail -n1")

	result=$(expr $result)

	reg='^[1-1]+$'
	if [[ 1 =~ $reg ]]; then
		log_message "Failed to create issuer: $issuerId - please check $logFile on the docgen server" "ERROR" "FILE" "" 1
		return 1
	fi

	result=$(scp ${vpnUser}@${docgenIp}:/etc/md/${site}/issuer/$issuer_id/issuer_passwords ./_log)

	if [ ! $? == 0 ];then
		log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	log_message "Successfully created issuer: $issuerId" "OK" "FILE" "" 1

	docgenCleanup=1

	return 0
}

enable_cem ()
{
	if [ -z $enableCem ]; then
		log_message "CEM diabled for $issuerId" "INFO" "FILE" "" 1
		return 0
	fi

	export logOut="/home/ecourier/ec_cem_issuer_enable_$issuerId.out"

        commands=(
        	"su - ecourier"
		". /etc/md/${site}/ecourier_shellrc"
		"touch ${log}"
		"/opt/md/custom/core360saas/bin/ec_cem_issuer_enable ${issuerId} --channel ${cemConf} > ${logOut} 2>&1"
	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" touch="${commands[2]}" cemEnable="${commands[3]}"  expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"
          
          set timeout -1
          expect -re $prompt
          send -- "$env(touch)\r" 

          set timeout -1
          expect -re $prompt
          send -- "$env(cemEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

	if [ ! $? == 0 ]; then
		log_message "Failed to enable CEM for: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	return 0
}

enable_ecm ()
{
	if [ -z $enableEcm ]; then
		log_message "ECM diabled for $issuerId" "INFO" "FILE" "" 1
		return 0
	fi

	echo "Code for ECM"

	return 0
}

mobetize ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "/opt/md/custom/core360saas/bin/ec_mobetize_issuer_enable ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" mobetizeEnable="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"
 
          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(mobetizeEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

	if [ ! $? == 0 ]; then
		log_message "Failed to enable mobetize." "ERROR" "FILE" "" 1
		return 1
	fi

	result=$(ssh -o LogLevel=QUIET -t -i ${privateKey} ${vpnUser}@${docgenIp} "echo '${issuerId}.* dtar,DAIMxdTransform' >> /etc/md/${site}/transforms/transform_bindings.kv")

	if [ ! $? == 0 ]; then
		log_message "Failed to update '/etc/md/${site}/transforms/transform_bindings.kv'" "ERROR" "FILE" "" 1
		return 1
	fi

	# Cant do final steps of mobetize creation, requires csrui interaction. Everything past the transform_bindings.kv steps will need to be done manually until there is an API to interact with the csrui - add this information and steps to the success text when the script finishes. 

	mobetizeNotice=1

	return 0

}

twillio_subaccount ()
{
	subaccountId="$issuerId-$description"

	# Check if SubAccount already exists (Closed accounts will never be deleted 
	# so we will need to append a number to the new subaccount if there was one 
	# closed already) 

	log_message "Checking subaccount id availability [$subaccountId]" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts.json?FriendlyName=$subaccountId&PageSize=20" -u "$twillioProductionSID:$twillioProductionToken")
	if [[ $result == *"first_page_uri"* ]]; then
		log_message "Subaccount already exists skipping creation [$subaccountId]" "OK" "FILE" "" 1
	else
		# Create SubAccount
		log_message "Attempting to create subaccount [$subaccountId]" "OK" "FILE" "" 1
		result=$(curl -X POST https://api.twilio.com/2010-04-01/Accounts.json --data-urlencode "FriendlyName=$subaccountId" -u "$twillioProductionSID:$twillioProductionToken")
		prettyResult=$(cat $result | python -m json.tool)

		if [[ $result == *"active"* ]]; then
			log_message "Successfully created subaccount: [$subaccountId]: $prettyResult" "OK" "FILE" "" 1
		else
			log_message "Failed to create subaccount: [$subaccountId]: $prettyResult" "ERROR" "FILE" "" 1
			return 1
		fi
	fi
	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A subArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		subArray[$key]=$value
       	done
	
	export subAccountSID=${subArray[sid]}

	# Get SubAccount AUTH Token
	log_message "Attempting to retrieve Sub-Account AUTH Token for Sub-Account: $subAccountSID" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts/$subAccountSID.json" -u "$twillioProductionSID:$twillioProductionToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to retrieve Sub-Account AUTH Token for Sub-Account: $subAccountSID" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A actArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		actArray[$key]=$value
	done

	export subAccountToken=${actArray[auth_token]}

	# Create Web Service
	requestUrl="InboundRequestUrl=$inboundRequestUrl"
	friendlyName="FriendlyName=$site messenging service"
	webHook="UseInboundWebhookOnNumber=true" 

	log_message "Attempting to create Web Service [$site messenging service]" "OK" "FILE" "" 1
	result=$(curl -X POST https://messaging.twilio.com/v1/Services -d $friendlyName -d $requestUrl -d $webHook -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to create Web Service [$site messenging service] - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A serviceArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		serviceArray[$key]=$value
	done

	serviceId=${serviceArray[sid]}
	log_message "Messeging Service ID: $serviceId" "INFO" "FILE" "" 1

	# Gather List of phone numbers
	log_message "Attempting to gather list of phone numbers in area code: $areaCode and country: $country" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts/$subAccountSID/AvailablePhoneNumbers/US/Local.json?AreaCode=$areaCode" -d "Type=local" -d "Geography.IsoCountry=$country" -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to gather list of phone numbers in area code: $areaCode and country: $country - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A numberList
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		numberList[$key]=$value
	done

	# Process number selection: present options or just choose one at random - ask paul

	### Make select number automatically - change to support regions not area code
	
	export number="chosen number from results +1xxxxxxxxxx"
	log_message "Selected number: $number" "INFO" "FILE" "" 1

	# Buy the selected number and add webservice to number
	log_message "Attempting to purchase number: $number" "OK" "FILE" "" 1
	result=$(curl -X POST https://api.twilio.com/2010-04-01/Accounts/$subAccountSID/IncomingPhoneNumbers.json -d "PhoneNumber=$number" -d "SmsApplicationSid=$serviceId" -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to purchase number: $number - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A completionArray	
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		completionArray[$key]=$value
	done

	status=${completionArray[status]}
	if [ ! $status == "in-use" ]; then
		log_message "Failed to purchase number: $number - $result" "ERROR" "FILE" "" 1
		return 1
	else
		log_message "Successfully purchased number: $number and configured to use Messeging Service: $serviceId" "OK" "FILE" "" 1
	fi

	return 0
}

twillio_conf ()
{
	siteConf="/etc/md/$site/site.conf.tcl"
	siteConfTemp="/etc/md/$site/site.conf.tcl.temp"
	twillioTcl="/etc/md/$site/issuer/$issuerId/ocm_channels/$issuerId_twilio_sms_channel.tcl"


        commands=(
		"cp ${siteConf} ${siteConfTemp}"
                "echo \"\# ${issuerId}_cem\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.account_auth_token   {907de40f44ef5710304f016fba75d4b5}\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.phone_number         \"${number}\"\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.issuer               \"${issuerId}\"\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.job_data_type        \"twilioSms\"\" >> ${siteConf}"
                "cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template_twilio_sms_channel.tcl ${twillioTcl}"
                "sed -i '6s/.*/set channel_id \"${issuerId}_twilio_sm\"/' ${twillioTcl}"
                "sed -i '11s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.issuer_id       \"${issuerId}\"/' ${twillioTcl}"
                "sed -i '13s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.account_sid     \"${subAccountSID}\"/' ${twillioTcl}"
                "sed -i '15s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.auth_token      \"${subAccountToken}\"/' ${twillioTcl}"
                "/etc/md/${site}/rc.ecourier restart"
                "/opt/md/custom/core360saas/bin/ec_build_wars"
                "/opt/md/custom/core360saas/bin/ec_copy_wars -s ${site}"
	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} cp="${commands[0]}" echo1="${commands[1]}" echo2="${commands[2]}" echo3="${commands[3]}" echo4="${commands[4]}" echo5="${commands[5]}" cp2="${commands[6]}" sed1="${commands[7]}" sed2="${commands[8]}" sed3="${commands[9]}" sed4="${commands[10]}" restart="${commands[11]}" buildWars="${commands[12]}" copyWars="${commands[13]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo5)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(restart)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(buildWars)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(copyWars)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

	if [ ! $? == 0 ]; then
		return 1
	fi

	cleanTwillio=1

	return 0
}

redeploy_wars ()
{
        commands=(
                "su - tomcat;"
                "/opt/md/custom/core360saas/bin/ectomcat_deploy_wars -l “ectwilio_sms_rest” -s /var/md/${site}/webapps;"
                "systemctl stop jws5-tomcat.service; systemctl start jws5-tomcat.service;"
        )

        env key=${privateKey} user=${vpnUser} ip=${webIp} pass=${vpnPass} su="${commands[0]}" deploy="${commands[1]}" restart="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(deploy)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(restart)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

	if [ ! $? == 0 ]; then
		return 1
	fi

	return 0
}

success_info ()
{
	log_message "Record the information from the following files and then delete the files '/etc/md/$site/issuer/$issuer_id/issuer_passwords' '$logOut'" "SUCCESS" "FILE" "" 1
	log_message "Create additional administrative users for the issuer. Log into the CSRUI and add any users required." "SUCCESS" "FILE" "" 1
	log_message "Setup Proxy (behind the elastic load balancer) in AWS to support DNS for the customer, at minimum we need to setup Twilio and tracking URLs." "SUCCESS" "FILE" "" 1
	log_message "Add the following DNS values for the issuer and validate them using a 3rd party tool like mxtoolbox.com" "SUCCESS" "FILE" "" 1

	printf "\nCNAME - used for DKIM keys to the global DKIM keys\n"
	printf "Domain				Name							Value\n"
	printf "$issuerId.doximapps.com		dxmselector1._domainkey.$issuerId.doximapps.com		dxmselector1._domainkey.doximapps.com\n"
	printf "$issuerId.doximapps.com         dxmselector2._domainkey.$issuerId.doximapps.com         dxmselector2._domainkey.doximapps.com\n\n"
	printf "MX - mail routing to the parent domain MTA\n"
	printf "Domain				Name							Value\n"
	printf "$issuerId.doximapps.com		mail.doximapps.com					10\n\n"
	printf "SPF - receiving MTAs accept mail from us\n"
	printf "Domain				TXT Recordm						Value\n"
	printf "$issuerId.doximapps.com		$issuerId.doximapps.com					v=spf1 a mx include: doximapps.com ~all\n"
	printf "NOTE: once this are running we will be changing ~all to -all\n\n"
	printf "DMARC - Route all reports back to us and give guidance to MTAs on delivery\n"
	printf "Domain				TXT Record 						Value\n"
	printf "$issuerId.doximapps.com		_dmarc.$issuerId.doximapps.com				v=DMARC1;"
	printf "											p=none;"
	printf "											rua=mailto:dmarcreports@$issuerId.doximapps.com;"
	printf "											ruf=mailto:dmarcsamples@$issuerId.doximapps.com;"
	printf "											fo=d:s;"

	if [ $mobetizeNotice ]; then
		printf "\n\nNOTICE: the mobetize setup is not complete. You must compelte the steps below to finalize the process.\n"
		printf "Now that the issuer is enabled with Mobetize, a CMS user in Mobetize will need to be created in the issuer so that bindings can be created. This new user should be given all of the CMS roles in the CSRUI (cms admin, cms template developer, cms template reviewer, cms binding developer, and cms binding reviewer).\n"
		printf "Once the user is created, we need to clone the mobetize templates to the issuer. This can be done by running the demLoadTool. Note, cloned templates from a Mobetize issuer is required before load can occur, see Appendix “Cloning the templates from Mobetize”.\n"
		printf "\n   /opt/md/bin/demLoadTool -c import -i <issuer_id> -d <full_path_to_templates>\n\n"
		printf "Review the logs in /var/md/<site>/log/demLoadTool_log.txt\n"
		printf "When the import is complete, an issuer property will be added to the issuer that will track the load of templates for historical information. This will be useful if Mobetize updates their templates and DevOps wants to know the last time a load occurred."
	fi
}

clean_up ()
{
	log_message "Starting clean up" "OK" "FILE" "" 1

	pyTemp="$BASE_DIRECTORY/temp.py"

	log_message "Cleaning up [$pyTemp]" "OK" "FILE" "" 1
	rm $pyTemp
	if [ ! $? == 0 ]; then
		log_message "Failed to delete [$pyTemp]" "ERROR" "FILE" "" 1	
		return 1
	else
		log_message "Deleted: [$pyTemp]" "OK" "FILE" "" 1
	fi

	if [ $docgenCleanup ]; then
        	file="${PWD}/script_tmp-docgen.sh"
		remoteFile="/home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"
		remoteFileTwo="/etc/md/${site}/issuer/$issuer_id/issuer_passwords"

		if [ -f $file ]; then
			log_message "Cleaning up [$file]" "OK" "FILE" "" 1
			
			rm -f $file
			if [ ! $? == 0 ]; then
				log_message "Failed to delete [$file]" "ERROR" "FILE" "" 1
				return 1
			fi
		fi

		log_message "Cleaning up [$remoteFile]" "OK" "FILE" "" 1

		ssh -i $privateKey "$vpnUser@$docgenIp" << EOF
rm $remoteFileTwo
rm $remoteFile
EOF
		if [ ! $? == 0 ]; then
			log_message "Failed to delete [$remoteFile]" "ERROR" "FILE" "" 1
			return 1
		fi

	fi

	if [ $cleanTwillio ]; then
		siteTemp="/etc/md/$site/site.conf.tcl.temp"

		log_message "Cleaning up [$siteTemp]" "OK" "FILE" "" 1
		ssh -i $privateKey "$vpnUser@$docgenIp" << EOF
rm $siteTemp
EOF
		if [ ! $? == 0 ]; then
			log_message "Failed to delete [$siteTemp]" "ERROR" "FILE" "" 1
			return 1
		fi

		log_message "Clean up complete" "OK" "FILE" "" 1

	fi

	return 0
}

vpn_check () 
{
	result=$(ifconfig | grep "10.21." | wc -l)
	if [ $result == 0 ]; then 
		return 1
	else
		return 0
	fi
}

rollback_database_users ()
{
        commands=(
                ". /home/db2inst1/sqllib/db2profile;"
                "cd /opt/md/custom/core360saas/bin/;"
                "./ecdb2_remove_issuer_users ${envPath}/ecdb2_environment.sh ${issuerId} ${db2Pass};"
        )

        env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} source="${commands[0]}" changeDir="${commands[1]}" remove="${commands[2]}" issuerRw="${issuerRWPass}" issuerRo="${issuerROPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(remove)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
        if [ ! $? == 0 ]; then
                log_message "Failed to rollback DB2 database users for $issuerId" "OK" "FILE" "" 1
                return 1
        fi

        log_message "Successfully rolled back DB2 database users for $issuerId" "OK" "FILE" "" 1
        return 0
}

rollback_docgen_setup ()
{
        cleanSaasConf="/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
	cleanCemConf="/home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"

	commands=(
		"rm -f ${cleanSaasConf}"
		"rm -f ${cleanCemConf}"
		"rm -f /home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "cd /opt/md/custom/core360saas/bin/"
                "./ec_remove_issuer ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} rm0="${commands[0]}" rm1="${commands[1]}" rm2="${commands[2]}" su="${commands[1]}" source="${commands[2]}" dir="${commands[3]}" remove="${commands[4]}" siteUser="${siteLevelUser}" sitePass="${siteLevelPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm0)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(dir)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(remove)\r"

          set timeout -1
          expect "*Enter your site level CORE360 username*"
          send -- "$env(siteUser)\r"

          set timeout -1
          expect "*Password: *"
          send -- "$env(sitePass)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to rollback issuer: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Sucessfully rolled back issuer: $issuerId" "OK" "FILE" "" 1
	return 0
}

rollback_enable_cem ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "touch ${log}"
                "/opt/md/custom/core360saas/bin/ec_cem_issuer_disable ${issuerId} --channel ${cemConf} >> ${logOut} 2>&1"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" touch="${commands[2]}" cemEnable="${commands[3]}"  expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"
          set timeout -1
          expect -re $prompt
          send -- "$env(touch)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(cemEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to roll back CEM for: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Successfully rolled back CEM for: $issuerId" "OK" "FILE" "" 1
        return 0
}

rollback_enable_ecm ()
{
	echo "Code for ECM rollback here."
}

rollback_mobetize ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "/opt/md/custom/core360saas/bin/ec_mobetize_issuer_disable ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" mobetizeEnable="${commands[2]}" expect << "EOS"          
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(mobetizeEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"
          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to rollback mobetize enable." "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Succesfully rolled back mobetize enable." "OK" "FILE" "" 1
	return 0
}

rollback_twillio_conf ()
{
	file="/etc/md/$site/issuer/$issuerId/ocm_channels/$issuerId_twilio_sms_channel.tcl"

        commands=(
                "rm -f $file;"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} rm="${commands[0]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof 
EOS
        if [ ! $? == 0 ]; then
	log_message "Failed to roll back Twillio Config" "ERROR" "FILE" "" 1

	fi

	log_message "Successfully rolled back Twillio Config" "OK" "FILE" "" 1
	return 0
}

force_delete_docgen ()
{
	 log_message "Force cleanup enabled for ${issuerId} - Cleaning configured Docgen Server" "OK" "FILE" "" 1

	 read -pw "Are you sure you want to forcibly delete docgen data for [${issuerId}]? This process should only be used if the issuer creation process failed during operation, casuing the standard issuer deletion method to not work. Please only run this if this is the case. You can not reverese this process. Continue? (Y/N):" -n 1 -r

	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		log_message "Force deletion for issuer [${issuerId}] cancelled by user." "INFO" "FILE" "" 1
		exit 0
	fi

	hash=$(echo $RANDOM | md5sum | head -c 20; echo;)

        # Remote cleanup of docgen
	commands=(
                "mkdir ~/backups-${hash}"
		"cp /etc/md/${site}/portdb.conf.tcl ~/backups-${hash}/portdb.conf.tcl"
		"CMD=\$(cat /etc/md/${site}/portdb.conf.tcl | grep 'issuer:${issuerId}')"
		"rm -rf /etc/md/${site}/issuer/${issuerId}/;"
                "rm -rf /var/md/${site}/issuer/${issuerId}/;"
                "rm -f /home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
                "rm -f /home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"
		"ASSIGNED=\$(cat /etc/md/${site}/portdb.conf.tcl | grep 'issuer:${issuerId}' | grep 'assigned');"
		"RESERVED=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'issuer:${issuerId}' | grep 'reserved');"
		"ASSIGNED_INDEX=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'portdb.assigned.index' | grep 'assigned');"
		"RESERVED_INDEX=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'portdb.reserved.index' | grep 'reserved');"
		"if [ $ASSIGNED_INDEX ]; then ASSIGNED_INDEX_NUM=\$(echo $ASSIGNED_INDEX | sed 's/[^0-9]*//g'); fi"
		"if [ $RESERVED_INDEX ]; then RESERVED_INDEX_NUM=\$(echo $RESERVED_INDEX | sed 's/[^0-9]*//g'); fi"
		"if [ $ASSIGNED_INDEX_NUM ]; then ASSIGNED_INDEX_NUM_I=$ASSIGNED_INDEX_NUM++;fi;"
		"if [ $RESERVED_INDEX_NUM ]; then RESERVED_INDEX_NUM_I=$RESERVED_INDEX_NUM++;fi;"
		"if [ $RESERVED ]; then RESERVED_NUM=\$(echo $RESERVED | sed 's/[^0-9]*//g') ;fi;"
		"if [ $ASSIGNED ]; then ASSIGNED_NUM=\$(echo $ASSIGNED | sed 's/[^0-9]*//g') ;fi;"
		"if [ $RESERVED ]; then sed -i '/portdb.reserved.${RESERVED_NUM}./d' /etc/md/${site}/portdb.conf.tcl; fi;"
		"if [ $ASSIGNED ]; then sed -i '/portdb.assigned.${ASSIGNED_NUM}./d' /etc/md/${site}/portdb.conf.tcl; fi;"
		"if [ $ASSIGNED_INDEX_NUM ]; then sed -i '/::md::optiondb add portdb.assigned.index                                        {${$ASSIGNED_INDEX_NUM}}/::md::optiondb add portdb.assigned.index                                        {${$ASSIGNED_INDEX_NUM_I}}/g /etc/md/siteca1/portdb.conf.tcl; fi;"
		"if [ $RESERVED_INDEX_NUM ]; then sed -i '/::md::optiondb add portdb.assigned.index                                        {${$RESERVED_INDEX_NUM}}/::md::optiondb add 3portdb.assigned.index                                        {${$RESERVED_INDEX_NUM_I}}/g /etc/md/siteca1/portdb.conf.tcl; fi;"

	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} mkdir="${commands[0]}" cp="${commands[1]}" portdb1="${commands[2]}" portdb2="${commands[3]}" portdb3="${commands[4]}" portdb4="${commands[5]}" portdb5="${commands[6]}" portdb6="${commands[7]}" portdb7="${commands[8]}" portdb8="${commands[9]}" portdb9="${commands[10]}" portdb10="${commands[11]}" portdb11="${commands[12]}" portdb12="${commands[13]}" portdb13="${commands[14]}" portdb14="${commands[15]}" portdb15="${commands[16]}" portdb16="${commands[17]}" portdb17="${commands[18]}" portdb18="${commands[19]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(mkdir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb5)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb6)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb7)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb8)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb9)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb10)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb11)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb12)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb13)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb14)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb15)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb16)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb17)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb18)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

}

force_delete_db ()
{
	log_message "Force cleanup enabled for ${issuerId} - Cleaning configured  - Cleaning configured DB2 Server Server" "OK" "FILE" "" 1

	read -p "Are you sure you want to forcibly delete DB2 data for [${issuerId}]? This process should only be used if the issuer creation process failed during operation, casuing the standard issuer deletion method to not work. Please only run this if this is the case. You can not reverese this process. Continue? (Y/N): " -n 1 -r

	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		log_message "Force deletion for issuer [${issuerId}] cancelled by user." "INFO" "FILE" "" 1
		exit 0
    	fi

	# We need to connect to the DB, delete issuer remnents.  

        commands=(
                " cd /home/db2inst1/sqllib"
		". db2profile"
		"./ecdb2_delete_issuer_users /root/ecdb2_environment.sh ${issuerId} ${db2Pass}"
        )

        env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} changeDir="${commands[0]}" source="${commands[1]}" deleteDb="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(deleteDb)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
        if [ ! $? == 0 ]; then
                log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
                return 1
        fi

	log_message "Force deletion of DB2 data for issuer [${issuerId}] successful." "OK" "FILE" "" 1
	return 0
}

next_available_port ()
{
        commands=(
		"CMD=\$(cat /etc/md/siteuatca1/portdb.conf.tcl | grep '::md::optiondb add portdb.reserved.' | grep 'start_port' | awk -F '{' '{gsub(\"}\",\"\");print \$2;}' | sort | tail -n1"
		"nextPort=\$((\$CMD + 100))"
		"echo \$nextPort >> /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt"
        )

	env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} grep="${commands[0]}" next="${commands[1]}" echo="${commands[1]}" expect << "EOS"	
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
	  send -- "$env(grep)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(next)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo)\r"	  
EOS

        if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	nextPort=$(ssh root@remote_maschine "cat /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt")

	if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	cleanup=$(ssh root@remote_maschine "rm -f /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt")

	if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	return $nextPort
}
