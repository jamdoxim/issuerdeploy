init ()
{
        yaml=$1

        # Validate command line arguments
        log_message "Validating provided arguments." "OK" "FILE" "" 1
        if [ ! -z $error ]; then
                log_message "Please only provde one argument. [YAML file path]" "ERROR" "FILE" "" 1
                return 1
        fi

        # Check for Python
        pythonVersion=$(python --version)
        if [[ ! $pythonVersion == *"Python 3"* ]]; then
                log_message "Script requires Python 3 or higher." "ERROR" "FILE" "" 1
                return 1
        fi

        # Verify YAML file
        if [ -z $yaml ]; then
                log_message "Specified file [$yaml] does not exist or is not readable." "ERROR" "FILE" "" 1
                return 1
        fi

	if ! validate_data $yaml; then
		return 1
	fi

        if ! process_yaml $yaml; then
		return 1
	fi

	if ! yaml_data; then
		return 1
	fi
	
	return 0
}

process_yaml ()
{
        yaml=$1

	log_message "Processing YAML file: [$yaml]" "INFO" "FILE" "" 1

	if [ ! -e $yaml ]; then
		log_message "Provided YAML file does not exist: [$yaml]" "ERROR" "FILE" "" 1
		return 1
	fi
	
	log_message "Processing YAML file: [$yaml]" "OK" "FILE" "" 1
	process=$(python -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < $yaml 2>&1)
        
	if [[ $process == *"Traceback"* ]] || [[ $process == *"No such file"* ]]; then
                log_message "Failed to process [$yaml]." "ERROR" "FILE" "" 1
                return 1
        else
		log_message "Processed YAML file: [$yaml]" "OK" "FILE" "" 1
		export yamlData=$process
	fi
	
	return 0
}

validate_data ()
{
        # YAML value sanitation and verification
        yaml=$1

	log_message "Validating data in provided YAML file: [$yaml]" "INFO" "FILE" "" 1

	testYaml=$(python -c 'import yaml, sys; print(yaml.safe_load(sys.stdin))' < $yaml 2>&1)

        if [[ $testYaml == *"Traceback"* ]]; then
                log_message "Specified file [$yaml] contains errors." "ERROR" "FILE" "" 1
                return 1
	fi

	log_message "Provided YAML file validated: [$yaml]" "OK" "FILE" "" 1

	return 0
}

yaml_data ()
{
	log_message "Attempting to process YAML Data" "INFO" "FILE" "" 1

	python='exec("""\nimport json,sys\ndef A():\n    C=str(sys.argv[1]);A=json.loads(C)\n    for B in A:D=A[B];print("{}={}".format(B,D)\n)\nif __name__=="__main__":A()\n""")'

	echo $python > $BASE_DIRECTORY/temp.py

	json=$(echo $yamlData | sed 's/'\''/"/g')

	data=$(python $BASE_DIRECTORY/temp.py "$json" 2>&1)

	if [[ $data == *"Traceback"* ]]; then
		log_message "Unable to process JSON data: $data" "ERROR" "FILE" "" 1
		return 1
	fi

	declare -A yamlArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		yamlArray[$key]=$value
	done

	export envPath=${yamlArray[envPath]}
	log_message "Enviroment Path: $envPath" "INFO" "FILE" "" 1
	
	export issuerId=${yamlArray[issuerId]}
	log_message "Issuer ID: $issuerId" "INFO" "FILE" "" 1

	export dbIp=${yamlArray[dbIp]}
	log_message "Database IP: $dbIp" "INFO" "FILE" "" 1

	export mtaIp=${yamlArray[mtaIp]}
	log_message "Mta IP: $mtaIp" "INFO" "FILE" "" 1
		
	export webIp=${yamlArray[webIp]}
	log_message "Web IP: $webIp" "INFO" "FILE" "" 1
		
	export appIp=${yamlArray[appIp]}
	log_message "App IP: $appIp" "INFO" "FILE" "" 1
		
	export docgenIp=${yamlArray[docgenIp]}
	log_message "Docgen IP: $docgenIp" "INFO" "FILE" "" 1

	export vpnUser=${yamlArray[vpnUser]}
	log_message "VPN Username: $vpnUser" "INFO" "FILE" "" 1

	export site=${yamlArray[site]}
	log_message "Deployment site: $site" "INFO" "FILE" "" 1

	export inboundRequestUrl=${yamlArray[inboundRequestUrl]}
	log_message "Inbound Request URL: $inboundRequestUrl" "INFO" "FILE" "" 1

	export twillioProductionSID=${yamlArray[twillioProductionSID]}
	log_message "Twillio account SID: $twillioProductionSID" "INFO" "FILE" "" 1
	
	export areaCode=${yamlArray[areaCode]}
	log_message "Area Code: $areaCode" "INFO" "FILE" "" 1

	export country=${yamlArray[country]}
	log_message "Country: $country" "INFO" "FILE" "" 1

        if [ ${yamlArray[mobetize]} == 'True' ]; then
		log_message "Mobetize Issuer = True - Note: manual steps required after completion." "INFO" "FILE" "" 1
		result=${yamlArray[mobetize]}
		export mobetize=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	if [ ${yamlArray[enableCem]} == 'True' ]; then
		log_message "CEM Enabled" "INFO" "FILE" "" 1
		result=${yamlArray[enableCem]}
		export enableCem=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	if [ ${yamlArray[enableEcm]} == 'True' ]; then
		log_message "ECM Enabled" "INFO" "FILE" "" 1
		result=${yamlArray[enableEcm]}
		export enableEcm=$(echo $result | tr '[:upper:]' '[:lower:]')
	fi

	export siteLevelUser=${yamlArray[siteLevelUser]}
	export siteLevelPass=${yamlArray[siteLevelPass]}

	export vpnPass=${yamlArray[vpnPass]}
	export db2Pass=${yamlArray[db2Pass]}
	export issuerRWPass=${yamlArray[issuerRWPass]}
	export issuerROPass=${yamlArray[issuerROPass]}
	export twillioProductionToken=${yamlArray[twillioProductionToken]}
	export doximDomain=${yamlArray[doximDomain]}

	export issuerFullName=${yamlArray[issuerFullName]}
	export issuerDescription=${yamlArray[issuerDescription]}
	export adminPrincipal=${yamlArray[adminPrincipal]}
	export adminFirstName=${yamlArray[adminFirstName]}
	export adminLastName=${yamlArray[adminLastName]}
	export adminPass=${yamlArray[adminPass]}

	export deployStages=${yamlArray[deployStages]}

	export forceDelete=${yamlArray[forceDelete]}
	if [ $forceDelete == 'True' ]; then
		if ! force_delete_docgen; then
			log_message "Failed to force delete issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			exit 1
		fi
		if ! force_delete_db; then
			log_message "Failed to force delete db2 data for issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			exit 1
		fi
		exit 0
	fi

	issuerPort=next_available_port
	if [ $issuerPort == 1 ]; then
		log_message "Failed to gather next port for issuer: [${issuerId}].." "ERROR" "FILE" "" 1
		return 1
	fi

	export port=$issuerPort

	log_message "Successfully processed YAML data" "OK" "FILE" "" 1

	return 0
}

main ()
{
        yaml=$1

        if [ ! $yaml ]; then
		log_message "Empty string provided for YAML file." "ERROR" "FILE" "" 1
		return 1
	fi

	if [ $yaml == "help" ]; then
		help_menu
		exit 0
	fi

        if ! init $yaml; then
		return 1
	fi
	
	if ! expect_check; then
		return 1
	fi

	if ! rsa_verify; then
		return 1
	fi

	STAGE=0

	if [ $forceDelete == 'True' ]; then
		STAGE="FORCE_DELETE"
		if ! force_delete_docgen; then
			log_message "Failed to force delete issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			return 1
		fi
		if ! force_delete_db; then
			log_message "Failed to force delete db2 data for issuer: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
			return 1
		fi
		if [ $force_deploy_wars ]; then
			if ! redeploy_wars; then
				log_message "Failed to redploy wars: [${issuerId}]. Please do cleanup process manually." "ERROR" "FILE" "" 1
				return 1
			fi
		fi
		return 0
	fi

	STAGE=1

	if [[ $deployStages == *"1"* ]]; then 
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! database_users; then
			rollback $STAGE
			return 1
		fi
	fi

	# Below here are untested

	STAGE=2
	if [[ $deployStages == *"2"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! docgen_setup; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=3

	if [[ $deployStages == *"3"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! enable_cem; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=4

	if [[ $deployStages == *"4"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! enable_ecm; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=5

	if [[ $deployStages == *"5"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		# Will need to test next time a mobetize issuer is required.
		if ! mobetize; then
			rollback $STAGE
			return 1
		fi
	fi

	STAGE=6

	if [[ $deployStages == *"6"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! twillio_subaccount; then
			rollback $STAGE
			return 1
		fi
	fi
	
	STAGE=7

	if [[ $deployStages == *"7"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! twillio_conf; then
			rollback $STAGE
			return 1
		fi
	fi
	STAGE=8

	if [[ $deployStages == *"8"* ]]; then
		log_message "Current stage: [${STAGE}]" "OK" "FILE" "" 1
		if ! redeploy_wars; then
			rollback $STAGE
			return 1
		fi
	fi

	return 0
}

help_menu ()
{
	printf "Issuer Deployment Tool\n\n"
	printf "usage: ./awsdeploy [YAML Config File]   	launch awsdeploy\n\n"

	printf "Arguments:\n"
	printf "  help                        			Access help menu and yaml configuration example.\n"

	printf "Directory Structure:\n"
	printf "  _lib                                          Holds all functions and scripts\n"
	printf "  _tempaltes                                    Holds all YAML template files\n"

	printf "Issuer Deployment Tool from anywhere:\n"
	printf "  Ensure permissions are correct      		[chmod u+x awsdeploy]\n"
	printf "  Add the following command to your .bashrc     [alias awsdeploy='<AWSDEPLOY ROOT>/awsdeploy']\n\n"

	# Add configuration example
}

rsa_verify ()
{
	user=$(whoami)

	log_message "Attempting to verify RSA access to required servers" "INFO" "FILE" "" 1
	log_message "Current detected user: $user" "INFO" "FILE" "" 1

	export privateKey="/home/$user/.ssh/id_issuerDeploy_rsa"

	# Add password pipe for id copy
	if [ ! -e $privateKey ]; then
		log_message "Private Key [$privateKey] does not exist. generating RSA" "INFO" "FILE" "" 1
		result=$(ssh-keygen -t rsa -f $privateKey -P '' 2>&1)
		if [[ ! $result == *"randomart"* ]]; then
			log_message "OpenSSL failed to create RSA key: $result" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "OpenSSL key created: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$dbIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			echo "$result"
			log_message "Failed to copy SSH key [$privateKey] to remote host: $dbIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $dbIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$docgenIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $docgenIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $docgenIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$webIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $webIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $webIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$appIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $appIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $appIp: $result" "OK" "FILE" "" 1
		fi

		result=$(./_lib/rsa.sh "$mtaIp" "$vpnUser" "$vpnPass" "$privateKey" 2>&1)
		if [[ ! $result == *"added: 1"* ]]; then
			log_message "Failed to copy SSH key [$privateKey] to remote host: $mtaIp" "ERROR" "FILE" "" 1
			return 1
		else
			log_message "Successfully copied SSH key [$privateKey] to remote host: $mtaIp: $result" "OK" "FILE" "" 1
		fi
	fi

	log_message "RSA file verification complete" "OK" "FILE" "" 1	
	
	return 0
}

expect_check ()
{
	log_message "Verifying required packages" "INFO" "FILE" "" 1
	
	unameOut="$(uname -s)"
	case "${unameOut}" in
    		Linux*)     machine=Linux;;
    		Darwin*)    machine=Mac;;
    		*)          machine="UNKNOWN:${unameOut}"
	esac

	if [[ $machine == "Linux" ]]; then
		if [ -e /etc/os-release ]; then
			if [ ! -e /usr/bin/expect ]; then
				log_message "Unable to find package 'expect', please install using: [apt-get install -y expect]" "ERROR" "FILE" "" 1
				return 1
			fi
		elif [ -e /etc/redhat-release ]; then
			if [ ! -e /usr/bin/expect ]; then
				log_message "Unable to find package 'expect', please install using: [yum install expect -y]" "ERROR" "FILE" "" 1
				return 1
			fi
		else
			log_message "Unable to determine Linux OS: expecting RedHat, or Ubuntu" "ERROR" "FILE" "" 1
			return 1
		fi
	elif [[ $machine == "Mac" ]]; then
		if [ ! -e /usr/bin/expect ]; then
			log_message "Unable to find package 'expect'" "ERROR" "FILE" "" 1
			return 1
		fi
	fi

	log_message "Required packages verified successfully" "OK" "FILE" "" 1

	return 0
}

success_info ()
{
	log_message "Record the information from the following files and then delete the files '/etc/md/$site/issuer/$issuer_id/issuer_passwords' '$logOut'" "SUCCESS" "FILE" "" 1
	log_message "Create additional administrative users for the issuer. Log into the CSRUI and add any users required." "SUCCESS" "FILE" "" 1
	log_message "Setup Proxy (behind the elastic load balancer) in AWS to support DNS for the customer, at minimum we need to setup Twilio and tracking URLs." "SUCCESS" "FILE" "" 1
	log_message "Add the following DNS values for the issuer and validate them using a 3rd party tool like mxtoolbox.com" "SUCCESS" "FILE" "" 1

	printf "\nCNAME - used for DKIM keys to the global DKIM keys\n"
	printf "Domain				Name							Value\n"
	printf "$issuerId.doximapps.com		dxmselector1._domainkey.$issuerId.doximapps.com		dxmselector1._domainkey.doximapps.com\n"
	printf "$issuerId.doximapps.com         dxmselector2._domainkey.$issuerId.doximapps.com         dxmselector2._domainkey.doximapps.com\n\n"
	printf "MX - mail routing to the parent domain MTA\n"
	printf "Domain				Name							Value\n"
	printf "$issuerId.doximapps.com		mail.doximapps.com					10\n\n"
	printf "SPF - receiving MTAs accept mail from us\n"
	printf "Domain				TXT Recordm						Value\n"
	printf "$issuerId.doximapps.com		$issuerId.doximapps.com					v=spf1 a mx include: doximapps.com ~all\n"
	printf "NOTE: once this are running we will be changing ~all to -all\n\n"
	printf "DMARC - Route all reports back to us and give guidance to MTAs on delivery\n"
	printf "Domain				TXT Record 						Value\n"
	printf "$issuerId.doximapps.com		_dmarc.$issuerId.doximapps.com				v=DMARC1;"
	printf "											p=none;"
	printf "											rua=mailto:dmarcreports@$issuerId.doximapps.com;"
	printf "											ruf=mailto:dmarcsamples@$issuerId.doximapps.com;"
	printf "											fo=d:s;"

	if [ $mobetizeNotice ]; then
		printf "\n\nNOTICE: the mobetize setup is not complete. You must compelte the steps below to finalize the process.\n"
		printf "Now that the issuer is enabled with Mobetize, a CMS user in Mobetize will need to be created in the issuer so that bindings can be created. This new user should be given all of the CMS roles in the CSRUI (cms admin, cms template developer, cms template reviewer, cms binding developer, and cms binding reviewer).\n"
		printf "Once the user is created, we need to clone the mobetize templates to the issuer. This can be done by running the demLoadTool. Note, cloned templates from a Mobetize issuer is required before load can occur, see Appendix “Cloning the templates from Mobetize”.\n"
		printf "\n   /opt/md/bin/demLoadTool -c import -i <issuer_id> -d <full_path_to_templates>\n\n"
		printf "Review the logs in /var/md/<site>/log/demLoadTool_log.txt\n"
		printf "When the import is complete, an issuer property will be added to the issuer that will track the load of templates for historical information. This will be useful if Mobetize updates their templates and DevOps wants to know the last time a load occurred."
	fi
}

clean_up ()
{
	log_message "Starting clean up" "OK" "FILE" "" 1

	pyTemp="$BASE_DIRECTORY/temp.py"

	log_message "Cleaning up [$pyTemp]" "OK" "FILE" "" 1
	rm $pyTemp
	if [ ! $? == 0 ]; then
		log_message "Failed to delete [$pyTemp]" "ERROR" "FILE" "" 1	
		return 1
	else
		log_message "Deleted: [$pyTemp]" "OK" "FILE" "" 1
	fi

	if [ $docgenCleanup ]; then
        	file="${PWD}/script_tmp-docgen.sh"
		remoteFile="/home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"
		remoteFileTwo="/etc/md/${site}/issuer/$issuer_id/issuer_passwords"

		if [ -f $file ]; then
			log_message "Cleaning up [$file]" "OK" "FILE" "" 1
			
			rm -f $file
			if [ ! $? == 0 ]; then
				log_message "Failed to delete [$file]" "ERROR" "FILE" "" 1
				return 1
			fi
		fi

		log_message "Cleaning up [$remoteFile]" "OK" "FILE" "" 1

		ssh -i $privateKey "$vpnUser@$docgenIp" << EOF
rm $remoteFileTwo
rm $remoteFile
EOF
		if [ ! $? == 0 ]; then
			log_message "Failed to delete [$remoteFile]" "ERROR" "FILE" "" 1
			return 1
		fi

	fi

	if [ $cleanTwillio ]; then
		siteTemp="/etc/md/$site/site.conf.tcl.temp"

		log_message "Cleaning up [$siteTemp]" "OK" "FILE" "" 1
		ssh -i $privateKey "$vpnUser@$docgenIp" << EOF
rm $siteTemp
EOF
		if [ ! $? == 0 ]; then
			log_message "Failed to delete [$siteTemp]" "ERROR" "FILE" "" 1
			return 1
		fi

		log_message "Clean up complete" "OK" "FILE" "" 1

	fi

	return 0
}

vpn_check () 
{
	result=$(ifconfig | grep "10.21." | wc -l)
	if [ $result == 0 ]; then 
		return 1
	else
		return 0
	fi
}

next_available_port ()
{
        commands=(
		"CMD=\$(cat /etc/md/siteuatca1/portdb.conf.tcl | grep '::md::optiondb add portdb.reserved.' | grep 'start_port' | awk -F '{' '{gsub(\"}\",\"\");print \$2;}' | sort | tail -n1"
		"nextPort=\$((\$CMD + 100))"
		"echo \$nextPort >> /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt"
        )

	env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} grep="${commands[0]}" next="${commands[1]}" echo="${commands[1]}" expect << "EOS"	
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
	  send -- "$env(grep)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(next)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo)\r"	  
EOS

        if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	nextPort=$(ssh root@remote_maschine "cat /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt")

	if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	cleanup=$(ssh root@remote_maschine "rm -f /home/${vpnUser}\\@cac1prod.doxim.net/temp-NEXTPORT-IssuerDeploy.txt")

	if [ ! $? == 0 ]; then
		log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
		return 1
	fi

	return $nextPort
}
