mobetize ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "/opt/md/custom/core360saas/bin/ec_mobetize_issuer_enable ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" mobetizeEnable="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"
 
          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(mobetizeEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

	if [ ! $? == 0 ]; then
		log_message "Failed to enable mobetize." "ERROR" "FILE" "" 1
		return 1
	fi

	result=$(ssh -o LogLevel=QUIET -t -i ${privateKey} ${vpnUser}@${docgenIp} "echo '${issuerId}.* dtar,DAIMxdTransform' >> /etc/md/${site}/transforms/transform_bindings.kv")

	if [ ! $? == 0 ]; then
		log_message "Failed to update '/etc/md/${site}/transforms/transform_bindings.kv'" "ERROR" "FILE" "" 1
		return 1
	fi

	# Cant do final steps of mobetize creation, requires csrui interaction. Everything past the transform_bindings.kv steps will need to be done manually until there is an API to interact with the csrui - add this information and steps to the success text when the script finishes. 

	mobetizeNotice=1

	return 0

}

