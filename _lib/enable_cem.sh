enable_cem ()
{
	if [ -z $enableCem ]; then
		log_message "CEM diabled for $issuerId" "INFO" "FILE" "" 1
		return 0
	fi

	export logOut="/home/ecourier/ec_cem_issuer_enable_$issuerId.out"

        commands=(
        	"su - ecourier"
		". /etc/md/${site}/ecourier_shellrc"
		"touch ${log}"
		"/opt/md/custom/core360saas/bin/ec_cem_issuer_enable ${issuerId} --channel ${cemConf} > ${logOut} 2>&1"
	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" touch="${commands[2]}" cemEnable="${commands[3]}"  expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"
          
          set timeout -1
          expect -re $prompt
          send -- "$env(touch)\r" 

          set timeout -1
          expect -re $prompt
          send -- "$env(cemEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

	if [ ! $? == 0 ]; then
		log_message "Failed to enable CEM for: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	return 0
}
