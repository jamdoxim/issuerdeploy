docgen_setup ()
{
	log_message "Configuring DocGen server - Issuer Creation" "INFO" "OK" "" 1

	# Build locally, scp over, run create - pipe all script commands to log, not main command

	saasConf="/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
	#export cemConf="/home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"
	export cemConf="${issuerId}_definition.conf.tcl"

	result=$(scp -i ${privateKey} _cem/${cemConf} ${vpnUser}@${docgenIp}:~/)

	#logFile="/home/${vpnUser}@${doximDomain}/deploy-${issuerId}.log"

	script_data="#!/bin/bash

TIMESTAMP=\$(date)

SAAS_CONF=\"/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl\"

if [ -f /home/ecourier/config/fusion_saas_issuer.conf.tcl ]; then
        error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Config file [/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl] already exists!\"
        echo \$error
        echo \$error >> $logFile
        echo \"1\" >> $logFile
        exit 1
fi

cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template.conf.tcl ${saasConf}]\"
	echo \$error
       	echo \$error >> $logFile
       	echo \"1\" >> $logFile
       	exit 1
fi

sed -i '9s/.*/set issuer_id \"${issuerId}\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 9]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '10s/.*/set mail_domain \"${issuerId}.doximapps.com\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 10]\"
	echo \$error
       	echo \$error >> $logFile
       	echo \"1\" >> $logFile
       	exit 1
fi

sed -i '13s/.*/md::optiondb add issuer.entity.fullname     \"${issuerFullName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 13]\"
	echo \$error
	echo \$error >> $logFileecho \"1\" >> $logFile
	exit 1
fi

sed -i '14s/.*/md::optiondb add issuer.entity.description  \"${issuerDescription}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 14]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '15s/.*/md::optiondb add issuer.entity.country_code ${country}/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 15]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '17s/.*/md::optiondb add issuer.administrator.principal  \"${adminPrincipal}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 17]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '18s/.*/md::optiondb add issuer.administrator.first_name \"${adminFirstName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then 
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 18]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '19s/.*/md::optiondb add issuer.administrator.last_name  \"${adminLastName}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 19]\"
	echo \$error
.	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '22s/.*/md::optiondb add issuer.administrator.password      \"${adminPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 22]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '23s/.*/md::optiondb add issuer.database.user.rw.password   \"${issuerRWPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 23]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '24s/.*/md::optiondb add issuer.database.user.ro.password   \"${issuerROPass}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 24]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

sed -i '28s/.*/md::optiondb add issuer.ports.start_port  \"${port}\"/' ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error modifying [line 28]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chown ecourier: ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chown ecourier: ${saasConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chmod 644 ${saasConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chmod 644 ${saasConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

chown ecourier: /home/${vpnUser}@${doximDomain}/${cemConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chown ecourier: /home/${vpnUser}@${doximDomain}/${cemConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile; exit 1
fi

chmod 644 /home/${vpnUser}@${doximDomain}/${cemConf}
if [ ! \$? == 0 ]; then
	error=\"\$TIMESTAMP - [ERROR] - Failed to create issuer ${issuerId} - Error running [chmod 644 /home/${vpnUser}@${doximDomain}/${cemConf}]\"
	echo \$error
	echo \$error >> $logFile
	echo \"1\" >> $logFile
	exit 1
fi

echo \"\$TIMESTAMP - [OK] - Successfully created issuer: [${issuerId}]\";
echo \"0\" >> $logFile
exit 0"

        file="${PWD}/script_tmp-docgen.sh"
	remoteFile="/home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"

	echo -e "$script_data" > $file

        if [ ! $? == 0 ];then
                log_message "Failed to create issuer: $issuerId - Unable to write to $file" "ERROR" "FILE" "" 1
                return 1
        fi

        result=$(scp -i ${privateKey} ${file} ${vpnUser}@${docgenIp}:~/)

        if [ ! $? == 0 ];then 
		log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	commands=(
		"chmod +x $remoteFile;" 
		"/usr/bin/bash /home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh >> /home/${vpnUser}@${doximDomain}/deploy-${issuerId}.log;" 
		"su - ecourier"
		". /etc/md/${site}/ecourier_shellrc"
		"cd /opt/md/custom/core360saas/bin/"
		"./ec_create_issuer ${saasConf}"
	)

	env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} chmod="${commands[0]}" script="${commands[1]}" su="${commands[2]}" source="${commands[3]}" dir="${commands[4]}" create="${commands[5]}" siteUser="${siteLevelUser}" sitePass="${siteLevelPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout 4
          expect "*s password:*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(chmod)\r"

          set timeout -1
	  expect -re $rootPrompt          
	  send -- "$env(script)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(dir)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(create)\r"

          set timeout -1
          expect "*Enter your site level CORE360 username*"
          send -- "$env(siteUser)\r"

          set timeout -1
          expect "*Password: *"
          send -- "$env(sitePass)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"
 
          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	result=$(ssh -o LogLevel=QUIET -t -i ${privateKey} ${vpnUser}@${docgenIp} "cat ${logFile} | tail -n1")

	result=$(expr $result)

	reg='^[1-1]+$'
	if [[ 1 =~ $reg ]]; then
		log_message "Failed to create issuer: $issuerId - please check $logFile on the docgen server" "ERROR" "FILE" "" 1
		return 1
	fi

	result=$(scp ${vpnUser}@${docgenIp}:/etc/md/${site}/issuer/$issuer_id/issuer_passwords ./_log)

	if [ ! $? == 0 ];then
		log_message "Failed to create issuer: $issuerId" "ERROR" "FILE" "" 1
		return 1
	fi

	log_message "Successfully created issuer: $issuerId" "OK" "FILE" "" 1

	docgenCleanup=1

	return 0
}
