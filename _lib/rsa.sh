#!/usr/bin/expect
set timeout 9
set hostname [lindex $argv 0]
set user [lindex $argv 1]
set password [lindex $argv 2]
set rsa [lindex $argv 3]

spawn ssh-copy-id -i $rsa $user@$hostname 

expect {
    timeout { send_user "\nFailed to get password prompt\n"; exit 1 }
    eof { send_user "\nRSA key added to: $hostname\n"; exit 1 }

    "*re you sure you want to continue connecting" {
        send "yes\r"
        exp_continue    
    }
    "*assword*" {
        send  "$password\r"
        interact
        exit 0
    }
}
