#!/usr/bin/expect
set timeout 9
set hostname [lindex $argv 0]
set user [lindex $argv 1]
set rsa [lindex $argv 2]
set envPath [lindex $argv 3]
set issuerId [lindex $argv 4]
set issuerRo [lindex $argv 5]
set db2Pass [lindex $argv 6]
set passwordSudo [lindex $argv 7]
set passwordId [lindex $argv 8]
set passwordRo [lindex $argv 9]
set script [lindex $argv 10]

spawn "ssh -i ${rsa} ${user}@${hostname} 'bash -s' < ${script}"

expect {
    timeout { send_user "\nSSH timeout: check VPN connection.\n"; exit 1 }
    eof { send_user "\nDone\n"; exit 0 }
}
