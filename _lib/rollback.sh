rollback ()
{
	rollbackStage=$1

	if [ $rollbackStage == 1 ]; then
		if ! rollback_database_users; then
			log_message "Error attempting to roll back Stage $rollbackStage [Rollback Database Users]" "ERROR" "FILE" "" 1		
			return 1
		fi
	fi

        if [ $rollbackStage == 2 ]; then
                if ! rollback_docgen_setup; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Docgen Setup]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 3 ]; then
                if ! rollback_enable_cem; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Enable CEM]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 4 ]; then
                if ! rollback_enable_ecm; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Enable ECM]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 5 ]; then
                if ! rollback_mobetize; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Mobetize Setup]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 6 ]; then
                if ! rollback_twillio_subaccount; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Twillio Subaccount Creation]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 7 ]; then
                if ! rollback_twillio_conf; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback ]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi

        if [ $rollbackStage == 8 ]; then
                if ! rollback_redeploy_wars; then
                        log_message "Error attempting to roll back Stage $rollbackStage [Rollback Database Users]" "ERROR" "FILE" "" 1
                        return 1
                fi
        fi
}

rollback_database_users ()
{
        commands=(
                ". /home/db2inst1/sqllib/db2profile;"
                "cd /opt/md/custom/core360saas/bin/;"
                "./ecdb2_remove_issuer_users ${envPath}/ecdb2_environment.sh ${issuerId} ${db2Pass};"
        )

        env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} source="${commands[0]}" changeDir="${commands[1]}" remove="${commands[2]}" issuerRw="${issuerRWPass}" issuerRo="${issuerROPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(remove)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
        if [ ! $? == 0 ]; then
                log_message "Failed to rollback DB2 database users for $issuerId" "OK" "FILE" "" 1
                return 1
        fi

        log_message "Successfully rolled back DB2 database users for $issuerId" "OK" "FILE" "" 1
        return 0
}

rollback_docgen_setup ()
{
        cleanSaasConf="/home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
	cleanCemConf="/home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"

	commands=(
		"rm -f ${cleanSaasConf}"
		"rm -f ${cleanCemConf}"
		"rm -f /home/${vpnUser}@${doximDomain}/script_tmp-docgen.sh"
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "cd /opt/md/custom/core360saas/bin/"
                "./ec_remove_issuer ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} rm0="${commands[0]}" rm1="${commands[1]}" rm2="${commands[2]}" su="${commands[1]}" source="${commands[2]}" dir="${commands[3]}" remove="${commands[4]}" siteUser="${siteLevelUser}" sitePass="${siteLevelPass}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm0)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(dir)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(remove)\r"

          set timeout -1
          expect "*Enter your site level CORE360 username*"
          send -- "$env(siteUser)\r"

          set timeout -1
          expect "*Password: *"
          send -- "$env(sitePass)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to rollback issuer: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Sucessfully rolled back issuer: $issuerId" "OK" "FILE" "" 1
	return 0
}

rollback_enable_cem ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "touch ${log}"
                "/opt/md/custom/core360saas/bin/ec_cem_issuer_disable ${issuerId} --channel ${cemConf} >> ${logOut} 2>&1"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" touch="${commands[2]}" cemEnable="${commands[3]}"  expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"
          set timeout -1
          expect -re $prompt
          send -- "$env(touch)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(cemEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to roll back CEM for: $issuerId" "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Successfully rolled back CEM for: $issuerId" "OK" "FILE" "" 1
        return 0
}

rollback_enable_ecm ()
{
	echo "Code for ECM rollback here."
}

rollback_mobetize ()
{
        commands=(
                "su - ecourier"
                ". /etc/md/${site}/ecourier_shellrc"
                "/opt/md/custom/core360saas/bin/ec_mobetize_issuer_disable ${issuerId}"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} su="${commands[0]}" source="${commands[1]}" mobetizeEnable="${commands[2]}" expect << "EOS"          
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(mobetizeEnable)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"
          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

        if [ ! $? == 0 ]; then
                log_message "Failed to rollback mobetize enable." "ERROR" "FILE" "" 1
                return 1
        fi

	log_message "Succesfully rolled back mobetize enable." "OK" "FILE" "" 1
	return 0
}

rollback_twillio_conf ()
{
	file="/etc/md/$site/issuer/$issuerId/ocm_channels/$issuerId_twilio_sms_channel.tcl"

        commands=(
                "rm -f $file;"
        )

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} rm="${commands[0]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(rm)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof 
EOS
        if [ ! $? == 0 ]; then
	log_message "Failed to roll back Twillio Config" "ERROR" "FILE" "" 1

	fi

	log_message "Successfully rolled back Twillio Config" "OK" "FILE" "" 1
	return 0
}
