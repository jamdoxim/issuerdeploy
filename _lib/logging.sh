log_devops_console ()
{
	SEVERITY=$1
	SUMMARY=$2
	LOG=$3
	orgShortName=$4
	ENV=$5
	DEPLOYCONFIG=$6
	SILO=$7
	PRODUCT=$8

	TIMESTAMP=$(date)
	ENDPOINT="https://devopsconsole.dxmapps.com/api/log/store"
	TOKEN=".token"

	if [ -z $TOKEN ]; then
		RESPONSE="Please configure the DevopsConsole api Token: [$TOKEN]"
		log_message $RESPONSE "ERROR" "FILE" "" 1
		return 1
	fi

	notifyAPIToken=$(cat $TOKEN)

	ACCEPT="Content-Type: application/json"
	AUTH="Authorization: Bearer $notifyAPIToken"

	RESPONSE=$(curl -X "POST" "https://devopsconsole.dxmapps.com/api/log/store" \
			-H 'Content-Type: application/json' \
			-H 'Authorization: Bearer HuOONqVB1oyvcodSMrAnJP2J8UxAU8AAzrinAHau3EvsxOxlzHtBuO6BBDaM' \
			-d $'{
					"summary": "'"$SUMMARY"'",
					"logEntries": "'"$LOG"'",
					"product": "'"$PRODUCT"'",
					"orgShortName": "'"$orgShortName"'",
					"env": "'"$ENV"'",
					"silo": "'"$SILO"'",
					"machineName": "'"$(hostname)"'",
					"severity": "'"$SEVERITY"'",
					"dateTime": "'"$TIMESTAMP"'",
					"deployConfig": "'"$DEPLOYCONFIG"'"
			}' 2>/dev/null)

	if [[ $RESPONSE == *"false"* ]]; then
		RESPONSE="Successful POST to: [$ENDPOINT]"
		log_message "$RESPONSE" "ERROR" "FILE" "" 1
	else
		RESPONSE="Failed to POST to: [$ENDPOINT]"
		log_message "$RESPONSE" "OK" "FILE" "" 1
		return 1
	fi

	return 0
}

log_enable ()
{
	# Define local log vars

	export LOG_DIR="$BASE_DIRECTORY/_log"
	export LOG_DEPLOYMENT="$BASE_DIRECTORY/_log/deployment"

	# Create log director if it does not exist
	if [ ! -d $LOG_DIR ]; then
		RESULT=$(mkdir -p $LOG_DIR 2>&1 )
		if [[ $RESULT == *"mkdir:"* ]]; then
			return 1 
		fi
		log_message "Created [$LOG_DIR]" "OK" "FILE" "" 1
	fi
	
	# Create log file if it does not exist
	if [ ! -f  $LOG_DEPLOYMENT ]; then
		touch $LOG_DEPLOYMENT
		log_message "Created [$LOG_DEPLOYMENT]" "OK" "FILE" "" 1
	fi

	return 0
}

log_file ()
{	
	SEVERITY=$1
	SUMMARY=$2
	LOG=$3
	
	TIMESTAMP=$(date)

	echo "$TIMESTAMP - [$SEVERITY] - $SUMMARY. $LOG" >> $LOG_DEPLOYMENT

	return 0
}

log_email ()
{
	# WIP
	
	SEVERITY=$1
	SUMMARY=$2
	LOG=$3
	ALERT_TO=$4

	mail -s "[$SEVERITY] - $SUMMARY" $ALERT_TO $LOG
	log_message $SUMMARY "OK" "FILE" "$LOG - [$ALERT_TO]" 1

	return 0
}

log_message ()
{
	SUMMARY=$1
	SEVERITY=$2
	CHANNELS=$3
	LOG=$4
	ECHO=$5
	orgShortName=$6
	ENV=$7
	DEPLOYCONFIG=$8
	SILO=$9

	if [ $SEVERITY == "OK" ]; then
		COLOR='\033[32;40m'
	elif [ $SEVERITY == "ERROR" ] || [ $SEVERITY == "FAIL" ]; then
		COLOR='\033[31;40m'
	elif [ $SEVERITY == "INFO" ]; then
		COLOR='\e[1;30m'	
	else
		COLOR='\033[36;40m'
	fi

        if [ $ECHO ]; then
		print_to_screen $SEVERITY "$SUMMARY" "$LOG" $COLOR
	fi

	if [[ $CHANNELS == *"API"* ]]; then
		log_devops_console $SEVERITY "$SUMMARY" "$LOG" $orgShortName $ENV $DEPLOYCONFIG $SILO
	elif [[ $CHANNELS == *"FILE"* ]]; then
		log_file $SEVERITY "$SUMMARY" "$LOG"
	elif [[ $CHANNELS == *"EMAIL"* ]]; then
		log_email $SEVERITY "$SUMMARY" "$LOG" "jam.legrand@doxim.com"
	fi

	return 0
}

print_to_screen ()
{
	SEVERITY=$1
	SUMMARY=$2
	LOG=$3
	COLOR=$4

	TIMESTAMP=$(date)
	COLOR_END='\e[0m'

	echo -e "$TIMESTAMP - [$SEVERITY] - $COLOR$SUMMARY. $LOG $COLOR_END"

	return 0
}
