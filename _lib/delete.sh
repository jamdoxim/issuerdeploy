force_delete_docgen ()
{
	 log_message "Force cleanup enabled for ${issuerId} - Cleaning configured Docgen Server" "OK" "FILE" "" 1

	 read -pw "Are you sure you want to forcibly delete docgen data for [${issuerId}]? This process should only be used if the issuer creation process failed during operation, casuing the standard issuer deletion method to not work. Please only run this if this is the case. You can not reverese this process. Continue? (Y/N):" -n 1 -r

	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		log_message "Force deletion for issuer [${issuerId}] cancelled by user." "INFO" "FILE" "" 1
		exit 0
	fi

	hash=$(echo $RANDOM | md5sum | head -c 20; echo;)

        # Remote cleanup of docgen
	commands=(
                "mkdir ~/backups-${hash}"
		"cp /etc/md/${site}/portdb.conf.tcl ~/backups-${hash}/portdb.conf.tcl"
		"CMD=\$(cat /etc/md/${site}/portdb.conf.tcl | grep 'issuer:${issuerId}')"
		"rm -rf /etc/md/${site}/issuer/${issuerId}/;"
                "rm -rf /var/md/${site}/issuer/${issuerId}/;"
                "rm -f /home/ecourier/config/${issuerId}_saas_issuer.conf.tcl"
                "rm -f /home/ecourier/config/cem_ecevent_${issuerId}_channel.conf.tcl"
		"ASSIGNED=\$(cat /etc/md/${site}/portdb.conf.tcl | grep 'issuer:${issuerId}' | grep 'assigned');"
		"RESERVED=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'issuer:${issuerId}' | grep 'reserved');"
		"ASSIGNED_INDEX=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'portdb.assigned.index' | grep 'assigned');"
		"RESERVED_INDEX=\$(cat /etc/md/siteca1/portdb.conf.tcl | grep 'portdb.reserved.index' | grep 'reserved');"
		"if [ $ASSIGNED_INDEX ]; then ASSIGNED_INDEX_NUM=\$(echo $ASSIGNED_INDEX | sed 's/[^0-9]*//g'); fi"
		"if [ $RESERVED_INDEX ]; then RESERVED_INDEX_NUM=\$(echo $RESERVED_INDEX | sed 's/[^0-9]*//g'); fi"
		"if [ $ASSIGNED_INDEX_NUM ]; then ASSIGNED_INDEX_NUM_I=$ASSIGNED_INDEX_NUM++;fi;"
		"if [ $RESERVED_INDEX_NUM ]; then RESERVED_INDEX_NUM_I=$RESERVED_INDEX_NUM++;fi;"
		"if [ $RESERVED ]; then RESERVED_NUM=\$(echo $RESERVED | sed 's/[^0-9]*//g') ;fi;"
		"if [ $ASSIGNED ]; then ASSIGNED_NUM=\$(echo $ASSIGNED | sed 's/[^0-9]*//g') ;fi;"
		"if [ $RESERVED ]; then sed -i '/portdb.reserved.${RESERVED_NUM}./d' /etc/md/${site}/portdb.conf.tcl; fi;"
		"if [ $ASSIGNED ]; then sed -i '/portdb.assigned.${ASSIGNED_NUM}./d' /etc/md/${site}/portdb.conf.tcl; fi;"
		"if [ $ASSIGNED_INDEX_NUM ]; then sed -i '/::md::optiondb add portdb.assigned.index                                        {${$ASSIGNED_INDEX_NUM}}/::md::optiondb add portdb.assigned.index                                        {${$ASSIGNED_INDEX_NUM_I}}/g /etc/md/siteca1/portdb.conf.tcl; fi;"
		"if [ $RESERVED_INDEX_NUM ]; then sed -i '/::md::optiondb add portdb.assigned.index                                        {${$RESERVED_INDEX_NUM}}/::md::optiondb add 3portdb.assigned.index                                        {${$RESERVED_INDEX_NUM_I}}/g /etc/md/siteca1/portdb.conf.tcl; fi;"

	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} mkdir="${commands[0]}" cp="${commands[1]}" portdb1="${commands[2]}" portdb2="${commands[3]}" portdb3="${commands[4]}" portdb4="${commands[5]}" portdb5="${commands[6]}" portdb6="${commands[7]}" portdb7="${commands[8]}" portdb8="${commands[9]}" portdb9="${commands[10]}" portdb10="${commands[11]}" portdb11="${commands[12]}" portdb12="${commands[13]}" portdb13="${commands[14]}" portdb14="${commands[15]}" portdb15="${commands[16]}" portdb16="${commands[17]}" portdb17="${commands[18]}" portdb18="${commands[19]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(mkdir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb5)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb6)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb7)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb8)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb9)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb10)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb11)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb12)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb13)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb14)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb15)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb16)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb17)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(portdb18)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

}

force_delete_db ()
{
	log_message "Force cleanup enabled for ${issuerId} - Cleaning configured  - Cleaning configured DB2 Server Server" "OK" "FILE" "" 1

	read -p "Are you sure you want to forcibly delete DB2 data for [${issuerId}]? This process should only be used if the issuer creation process failed during operation, casuing the standard issuer deletion method to not work. Please only run this if this is the case. You can not reverese this process. Continue? (Y/N): " -n 1 -r

	if [[ ! $REPLY =~ ^[Yy]$ ]]; then
		log_message "Force deletion for issuer [${issuerId}] cancelled by user." "INFO" "FILE" "" 1
		exit 0
    	fi

	# We need to connect to the DB, delete issuer remnents.  

        commands=(
                " cd /home/db2inst1/sqllib"
		". db2profile"
		"./ecdb2_delete_issuer_users /root/ecdb2_environment.sh ${issuerId} ${db2Pass}"
        )

        env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} changeDir="${commands[0]}" source="${commands[1]}" deleteDb="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(deleteDb)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
        if [ ! $? == 0 ]; then
                log_message "Failed to force delete Db2 data for [${issuerId}]" "OK" "FILE" "" 1
                return 1
        fi

	log_message "Force deletion of DB2 data for issuer [${issuerId}] successful." "OK" "FILE" "" 1
	return 0
}

