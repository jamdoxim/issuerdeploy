twillio_subaccount ()
{
	subaccountId="$issuerId-$description"

	# Check if SubAccount already exists (Closed accounts will never be deleted 
	# so we will need to append a number to the new subaccount if there was one 
	# closed already) 

	log_message "Checking subaccount id availability [$subaccountId]" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts.json?FriendlyName=$subaccountId&PageSize=20" -u "$twillioProductionSID:$twillioProductionToken")
	if [[ $result == *"first_page_uri"* ]]; then
		log_message "Subaccount already exists skipping creation [$subaccountId]" "OK" "FILE" "" 1
	else
		# Create SubAccount
		log_message "Attempting to create subaccount [$subaccountId]" "OK" "FILE" "" 1
		result=$(curl -X POST https://api.twilio.com/2010-04-01/Accounts.json --data-urlencode "FriendlyName=$subaccountId" -u "$twillioProductionSID:$twillioProductionToken")
		prettyResult=$(cat $result | python -m json.tool)

		if [[ $result == *"active"* ]]; then
			log_message "Successfully created subaccount: [$subaccountId]: $prettyResult" "OK" "FILE" "" 1
		else
			log_message "Failed to create subaccount: [$subaccountId]: $prettyResult" "ERROR" "FILE" "" 1
			return 1
		fi
	fi
	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A subArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		subArray[$key]=$value
       	done
	
	export subAccountSID=${subArray[sid]}

	# Get SubAccount AUTH Token
	log_message "Attempting to retrieve Sub-Account AUTH Token for Sub-Account: $subAccountSID" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts/$subAccountSID.json" -u "$twillioProductionSID:$twillioProductionToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to retrieve Sub-Account AUTH Token for Sub-Account: $subAccountSID" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A actArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		actArray[$key]=$value
	done

	export subAccountToken=${actArray[auth_token]}

	# Create Web Service
	requestUrl="InboundRequestUrl=$inboundRequestUrl"
	friendlyName="FriendlyName=$site messenging service"
	webHook="UseInboundWebhookOnNumber=true" 

	log_message "Attempting to create Web Service [$site messenging service]" "OK" "FILE" "" 1
	result=$(curl -X POST https://messaging.twilio.com/v1/Services -d $friendlyName -d $requestUrl -d $webHook -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to create Web Service [$site messenging service] - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A serviceArray
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		serviceArray[$key]=$value
	done

	serviceId=${serviceArray[sid]}
	log_message "Messeging Service ID: $serviceId" "INFO" "FILE" "" 1

	# Gather List of phone numbers
	log_message "Attempting to gather list of phone numbers in area code: $areaCode and country: $country" "OK" "FILE" "" 1
	result=$(curl -X GET "https://api.twilio.com/2010-04-01/Accounts/$subAccountSID/AvailablePhoneNumbers/US/Local.json?AreaCode=$areaCode" -d "Type=local" -d "Geography.IsoCountry=$country" -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to gather list of phone numbers in area code: $areaCode and country: $country - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A numberList
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		numberList[$key]=$value
	done

	# Process number selection: present options or just choose one at random - ask paul

	### Make select number automatically - change to support regions not area code
	
	export number="chosen number from results +1xxxxxxxxxx"
	log_message "Selected number: $number" "INFO" "FILE" "" 1

	# Buy the selected number and add webservice to number
	log_message "Attempting to purchase number: $number" "OK" "FILE" "" 1
	result=$(curl -X POST https://api.twilio.com/2010-04-01/Accounts/$subAccountSID/IncomingPhoneNumbers.json -d "PhoneNumber=$number" -d "SmsApplicationSid=$serviceId" -u "$subAccountSID:$subAccountToken")
	if [ ! $? == 0 ]; then
		log_message "Failed to purchase number: $number - $result" "ERROR" "FILE" "" 1
		return 1
	fi

	data=$(python $BASE_DIRECTORY/temp.py "$result" 2>&1)

	declare -A completionArray	
	for i in $data; do
		key=$(echo $i | awk -F '=' '{print $1}')
		value=$(echo $i | awk -F '=' '{print $2}')

		completionArray[$key]=$value
	done

	status=${completionArray[status]}
	if [ ! $status == "in-use" ]; then
		log_message "Failed to purchase number: $number - $result" "ERROR" "FILE" "" 1
		return 1
	else
		log_message "Successfully purchased number: $number and configured to use Messeging Service: $serviceId" "OK" "FILE" "" 1
	fi

	return 0
}
