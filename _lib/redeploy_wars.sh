redeploy_wars ()
{
        commands=(
                "su - tomcat;"
                "/opt/md/custom/core360saas/bin/ectomcat_deploy_wars -l “ectwilio_sms_rest” -s /var/md/${site}/webapps;"
                "systemctl stop jws5-tomcat.service; systemctl start jws5-tomcat.service;"
        )

        env key=${privateKey} user=${vpnUser} ip=${webIp} pass=${vpnPass} su="${commands[0]}" deploy="${commands[1]}" restart="${commands[2]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(su)\r"

          set timeout -1
          expect -re $prompt
          send -- "$env(deploy)\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(restart)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS

	if [ ! $? == 0 ]; then
		return 1
	fi

	return 0
}
