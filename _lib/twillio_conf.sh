twillio_conf ()
{
	siteConf="/etc/md/$site/site.conf.tcl"
	siteConfTemp="/etc/md/$site/site.conf.tcl.temp"
	twillioTcl="/etc/md/$site/issuer/$issuerId/ocm_channels/$issuerId_twilio_sms_channel.tcl"


        commands=(
		"cp ${siteConf} ${siteConfTemp}"
                "echo \"\# ${issuerId}_cem\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.account_auth_token   {907de40f44ef5710304f016fba75d4b5}\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.phone_number         \"${number}\"\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.issuer               \"${issuerId}\"\" >> ${siteConf}"
                "echo \"::md::optiondb add ectwilio.sms_rest.inbound.phone_numbers.comtechfire_cem.job_data_type        \"twilioSms\"\" >> ${siteConf}"
                "cp /opt/md/custom/core360saas/libdata/core360_saas_issuer_template_twilio_sms_channel.tcl ${twillioTcl}"
                "sed -i '6s/.*/set channel_id \"${issuerId}_twilio_sm\"/' ${twillioTcl}"
                "sed -i '11s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.issuer_id       \"${issuerId}\"/' ${twillioTcl}"
                "sed -i '13s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.account_sid     \"${subAccountSID}\"/' ${twillioTcl}"
                "sed -i '15s/.*/::md::optiondb add ocmd.channel.$\{channel_id\}.auth_token      \"${subAccountToken}\"/' ${twillioTcl}"
                "/etc/md/${site}/rc.ecourier restart"
                "/opt/md/custom/core360saas/bin/ec_build_wars"
                "/opt/md/custom/core360saas/bin/ec_copy_wars -s ${site}"
	)

        env key=${privateKey} user=${vpnUser} ip=${docgenIp} pass=${vpnPass} cp="${commands[0]}" echo1="${commands[1]}" echo2="${commands[2]}" echo3="${commands[3]}" echo4="${commands[4]}" echo5="${commands[5]}" cp2="${commands[6]}" sed1="${commands[7]}" sed2="${commands[8]}" sed3="${commands[9]}" sed4="${commands[10]}" restart="${commands[11]}" buildWars="${commands[12]}" copyWars="${commands[13]}" expect << "EOS"
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout -1
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(echo5)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(cp2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed1)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed2)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed3)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(sed4)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(restart)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(buildWars)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(copyWars)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof
EOS

	if [ ! $? == 0 ]; then
		return 1
	fi

	cleanTwillio=1

	return 0
}
