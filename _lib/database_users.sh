database_users ()
{
	log_message "Attempting to create DB2 database users for DB Server: $dbIp using the username: $vpnUser" "INFO" "OK" "" 1
	#result=$(timeout 10 | ssh -i ${privateKey} ${vpnUser}@${dbIp} "su $issuerId" 2>&1)
	#if [[ ! "$result" == *"does not exist"* ]];then
	#	log_message "Failed to create DB2 database users for $issuerId - user already exists" "ERROR" "FILE" "" 1
	#	return 1
	#fi

	issuerRo=$( printf $issuerId;printf "ro" )

	commands=(
		". /home/db2inst1/sqllib/db2profile;"
		"cd /opt/md/custom/core360saas/bin/;"
		"./ecdb2_create_issuer_users ${envPath}/ecdb2_environment.sh ${issuerId} ${db2Pass};"
		"passwd ${issuerId}"
		"passwd ${issuerId}ro"
	)

	env key=${privateKey} user=${vpnUser} ip=${dbIp} pass=${vpnPass} source="${commands[0]}" changeDir="${commands[1]}" create="${commands[2]}" passOne="${commands[3]}" passTwo="${commands[4]}" issuerRw="${issuerRWPass}" issuerRo="${issuerROPass}" expect << "EOS" 
          # Prompt for password, cribbed/converted from example on expect(1) manpage
          set prompt {\]\$}
          set rootPrompt {\]\#}

          spawn -noecho sudo -k
          spawn ssh -o LogLevel=QUIET -t -i $env(key) $env(user)@$env(ip)

          set timeout 4
	  expect "*s password:*"
	  send -- "$env(pass)\r"

          set timeout -1
          expect -re $prompt
          send -- "sudo -s\r"

          set timeout 4
          expect "*password for*"
          send -- "$env(pass)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(source)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(changeDir)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(create)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "$env(passOne)\r"

          set timeout 4
          expect "*New password*"
          send -- "$env(issuerRw)\r"

          set timeout 4
          expect "*Retype new password*"
          send -- "$env(issuerRw)\r"

	  set timeout -1
          expect -re $rootPrompt
          send -- "$env(passTwo)\r"

          set timeout 4
          expect "*New password*"
          send -- "$env(issuerRo)\r"

          set timeout 4
          expect "*Retype new password*"
          send -- "$env(issuerRo)\r"

          set timeout -1
          expect -re $rootPrompt
          send -- "exit\r"

          set timeout -1
          expect -re $prompt
          send -- "exit\r"

          set timeout -1
          expect eof

EOS
	if [ ! $? == 0 ]; then
		log_message "Failed to create DB2 database users for $issuerId" "OK" "FILE" "" 1
		return 1
	fi

	log_message "Successfully created DB2 database users for $issuerId" "OK" "FILE" "" 1
	return 0
}
